#pragma once

#include <iostream>

class Type
{
public:
	int _member;

	Type()
	{
		this->_member = 0;
	}

	Type(int member)
	{
		this->_member = member;
	}

	~Type()
	{
	}

	//operators

	Type& operator=(const Type& other)
	{
		this->_member = other._member;
		return *this;
	}

	friend bool operator<(const Type& first, const Type& second);

	friend std::ostream& operator<<(std::ostream& output, Type t);

	friend bool operator==(const Type& first, const Type& second);
};

bool operator<(const Type& first, const Type& second)
{
	return first._member < second._member;
}

std::ostream& operator<<(std::ostream& output, Type t)
{
	output << t._member;
	return output;
}

bool operator==(const Type& first, const Type& second)
{
	return first._member == second._member;
}
