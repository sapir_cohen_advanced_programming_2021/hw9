#pragma once

#include <iostream>>

enum res{BIGGER = -1, EQUAL, SMALLER};

template <class T>
int compare(T first, T second)
{
	if (first < second) return SMALLER;
	else if (first == second) return EQUAL;
	else return BIGGER;
}

template <class T>
void bubbleSort(T arr[], int size)
{
	T temp;
	for (int i = 0; i < size - 1; i++) for (int j = 0; j < size - i- 1; j++)
	{
		if (compare(arr[j], arr[j + 1]) == BIGGER)
		{
			temp = arr[j];
			arr[j] = arr[j + 1];
			arr[j + 1] = temp;
		}
	}
}

template <class T>
void printArray(T arr[], int size)
{
	for (int i = 0; i < size; i++) std::cout << arr[i] << std::endl;
}

template <class T>
void printArraySameLine(T arr[], int size)
{
	for (int i = 0; i < size; i++) std::cout << arr[i] << " ";
}