#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"

using std::cout;
using std::endl;

int main()
{
	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("5");
	bs->insert("6");
	bs->insert("3");
	bs->insert("8");
	bs->insert("2");
	/*bs->insert("2");
	bs->insert("8");
	bs->insert("9");
	bs->insert("7");
	bs->insert("4");
	bs->insert("3");*/

	cout << "Check if 8 in tree: " << bs->search("8") << endl;
	cout << "Check if 1 in tree: " << bs->search("1") << endl;

	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;

	BSNode* bsCopy = bs;

	bsCopy->printNodes();

	std::string textTree = "BSTData.txt";
	printTreeToFile(bsCopy, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());

	return 0;
}
