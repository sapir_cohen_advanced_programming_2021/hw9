#include <iostream>
#include "BSNodeT.h"
#include "functions.h"

template <class T>
BSNode<T>& putInBSNodes(T arr[], int size)
{
	BSNode<T>* bsT= new BSNode<T>(arr[0]);
	for(int i=1;i<size;i++) bsT->insert(arr[i]);
	return *bsT;
}

/*int main()
{
	const int size = 15;
	int intArr[size] = {1,897,32,52,68,13,75,12,8,32,7,9,5,8,10};
	std::string strArr[size] = {"abc","zfe","yr","fs","Yew","usagr","rrye", "tu","yuikj","cbs","ipIu","saw","trjy", "nsefs","kfd"};

	printArraySameLine(intArr, size);
	std::cout << std::endl;
	printArraySameLine(strArr, size);
	std::cout << std::endl;

	BSNode<int> bsInt = putInBSNodes(intArr, size);
	BSNode<std::string> bsStr = putInBSNodes(strArr, size);
	
	bsInt.printNodes();
	std::cout << std::endl;
	bsStr.printNodes();

	return 0;
}*/