#include "BSNode.h"

BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_right = this->_left = nullptr;
	this->_count = 1;
}

BSNode::BSNode(const BSNode& other)
{
	this->_data = other.getData();
	this->_count = other._count;
	if (other.getRight() != nullptr) this->_right = new BSNode(*other.getRight());
	else this->_right = nullptr;
	if (other.getLeft() != nullptr) this->_left = new BSNode(*other.getLeft());
	else this->_left = nullptr;
}

BSNode::~BSNode()
{
	if (this->_right != nullptr) this->_right->~BSNode();
	else delete this->_right;
	if (this->_left != nullptr) this->_left->~BSNode();
	else delete this->_left;
}

void BSNode::insert(std::string value)
{
	if (this->_data == value)
	{
		this->_count++;
		return;
	}
	if (this->_data < value)
	{
		if (this->_right != nullptr) this->_right->insert(value);
		else this->_right = new BSNode(value);
	}
	else
	{
		if (this->_left != nullptr) this->_left->insert(value);
		else this->_left = new BSNode(value);
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	*this = BSNode(other);
	return *this;
}

bool BSNode::isLeaf() const
{
	return this->_right == nullptr && this->_left == nullptr;
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(std::string val) const
{
	if (this->_data == val) return true;
	else if (this->_data < val && this->_right != nullptr) return this->_right->search(val);
	else if(this->_left != nullptr) return this->_left->search(val);
	return false;
}

int BSNode::getHeight() const
{
	if (this == nullptr || this->isLeaf()) return 0;
	else
	{
		if (this->_right->getHeight() + 1 > this->_left->getHeight() + 1) return this->_right->getHeight() + 1;
		else return this->_left->getHeight() + 1;
	}
}

int BSNode::getDepth(const BSNode& root) const 
{
	if (root.search(this->_data))
	{
		if (this->_data == root.getData()) return 0;
		else if (root.getData() < this->_data) return this->getDepth(*root.getRight()) + 1;
		else return this->getDepth(*root.getLeft()) + 1;
	}
	else return ERROR_RET;
}

void BSNode::printNodes() const
{
	if (this != nullptr)
	{
		if(this->_left != nullptr) this->_left->printNodes();
		std::cout << this->_count << " " << this->_data << std::endl;
		if(this->_right != nullptr) this->_right->printNodes();
	}
}
